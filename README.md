![N|Solid](logo_ioasys.png)

# README #

Estes documento README tem como objetivo fornecer as informações necessárias pedidas no projeto Empresas.


### Dependencias ###

* dependências de material, para seguir o padrao de material designer imposto pela google.

* dependências de lifecycle, foram utilizadas para ter acesso as viewmodel, acesso independente da reconstrucao de tela,
eu sempre vou ter acesso as minhas viewmodel, porem nao utilizei o livedata por utilizar o flow com o collectAsState que se encaixa e é indicada para a programacao reativa com o compose

* dependências de coil, foram utilizadas para que a toolbar nao obrescreva o statusbar, pois o compose nao tem um suporte de facil manipulacao disso de forma nativa

* dependências de retrofit, realizar chamadas externa http ou https para o servidor e recuperar os dados

* dependências de assertj, realizar testes unitarios para comparacoes de valores e objetos

* dependências de mockito, realizar testes unitarios para validar se as devidas chamadas foram feitas e mocar objetos e chamadas

* dependências de mockk, realizar testes unitarios para mocar objetos

### Se tivesse mais tempo ###

* Se eu tivesse mais tempo, focaria em melhorar os pequenos detalhes que fossem necessários,
e realizaria a implementacao de dagger2 ou coin para injecao de dependencia,
onde os repositorios e os usecases e viewmodel foram feitos para suportarem injecao de dependecia pelo construir,
e tambem faria os testes unitarios da viewmodel e das screens e dos componentes em compose utilizando o recursos como o composeRule,
tambem tornaria os pacotes de commons,coreapi e dsc como modulos independentes.

### Como executar a aplicacao ###
* Abrir o pacote ioasys no Android studio, escolher um emulador e clicar no play apos o reconhecimento de android studio,
ou instalar o apk gerado na pasta raiz

### Dados para Teste de Login ###

* Usuário de Teste: testeapple@ioasys.com.br
* Senha de Teste : 12341234

### Bônus ###

* (x) Testes unitários, pode usar a ferramenta que você tem mais experiência, só nos explique o que ele tem de bom.
    * Utilizei o mockito, mockk e o assertj para realizacao dos testes unitarios;
* (x) Usar uma arquitetura testável. Ex: MVP, MVVM, Clean, etc.
    * Utilizei o MVVM seguindo o clean architecture, por se encaixa com o padrao de programacao reativa do compose juntamente com o flow com o collectAsState,
    e com a viewmodel do lifecycle, sendo uma arquitetura de facil teste ja que todas as logicas ficaram na viewmodel;
* (x) Material Design
    * decidi nao seguir os espacamentos proposto no figma pois estao fora do padrao do Material Design que seria exponencial a 2 (exemplo: 2dp, 4dp, 8dp, 16dp...),
    os componentes utilizando o compose tambem seguem fortemente o padrao de Material Design;
* () Utilizar alguma ferramenta de Injeção de Dependência, Dagger, Koin e etc..
    * nao foi utilizado por falta de tempo para implementacao (pascoa);
* (x) Utilizar Rx, LiveData, Coroutines.
    * Utilizei o Flow com o collectAsState ao inves do LiveData por se encaixar melhor com o compose e tambem Coroutines para as chamadas externa com funcoes suspend;
* Padrões de projetos
    * Observable para as chamadas via Flow com o collectAsState,
    Builder para os estados da tela,
    Singleton para armazenamento dos dados da secao, entre outros...;
