package com.teste.ioasys.application.domain.model

import com.teste.ioasys.commons.extensions.EMPTY_STRING

data class Enterprise(
    val id: Int = 0,
    val name: String = EMPTY_STRING,
    val description: String = EMPTY_STRING,
    val photo: String = EMPTY_STRING,
    val country: String = EMPTY_STRING,
    val type: EnterpriseType = EnterpriseType(),
) {
    companion object {
        fun mock() = Enterprise(
            id = 1,
            name = "name",
            description = "desc",
            photo = "www",
            country = "BR",
            type = EnterpriseType.mock()
        )

        fun mocks() = listOf(
            Enterprise(
                id = 1,
                name = "name",
                description = "desc",
                photo = "www",
                country = "BR",
                type = EnterpriseType.mock()
            )
        )
    }
}
