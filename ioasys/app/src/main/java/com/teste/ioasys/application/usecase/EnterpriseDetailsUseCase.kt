package com.teste.ioasys.application.usecase

import com.teste.ioasys.application.data.repository.EnterprisesRepositoryImpl
import com.teste.ioasys.application.domain.repository.EnterprisesRepository
import com.teste.ioasys.commons.extensions.safeRunDispatcher

class EnterpriseDetailsUseCase(
    val enterprisesRepository: EnterprisesRepository = EnterprisesRepositoryImpl()
) {

    suspend fun execute(
        id: Int
    ) = safeRunDispatcher {
        enterprisesRepository.detail(id = id)
    }
}