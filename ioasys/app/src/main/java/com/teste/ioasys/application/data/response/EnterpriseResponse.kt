package com.teste.ioasys.application.data.response

import com.teste.ioasys.commons.extensions.EMPTY_STRING

data class EnterpriseResponse(
    val id: Int = 0,
    val enterprise_name: String = EMPTY_STRING,
    val description: String = EMPTY_STRING,
    val photo: String = EMPTY_STRING,
    val country: String = EMPTY_STRING,
    val enterprise_type: EnterpriseTypeResponse = EnterpriseTypeResponse(),
) {
    companion object {
        fun mock() = EnterpriseResponse(
            id = 1,
            enterprise_name = "name",
            description = "desc",
            photo = "www",
            country = "BR",
            enterprise_type = EnterpriseTypeResponse.mock()
        )

        fun mocks() = listOf(
            EnterpriseResponse(
                id = 1,
                enterprise_name = "name",
                description = "desc",
                photo = "www",
                country = "BR",
                enterprise_type = EnterpriseTypeResponse.mock()
            )
        )
    }
}
