package com.teste.ioasys.dsc.color

import androidx.compose.runtime.Composable
import androidx.compose.ui.res.colorResource
import com.teste.ioasys.R

object ColorPalette {

    val Transparent @Composable get() = colorResource(id = android.R.color.transparent)
    val White @Composable get() = colorResource(id = android.R.color.white)

    val MediumPink @Composable get() = colorResource(id = R.color.medium_pink)
    val Beige @Composable get() = colorResource(id = R.color.beige)
    val GreenyBlue @Composable get() = colorResource(id = R.color.greeny_blue)
    val CharcoalGrey @Composable get() = colorResource(id = R.color.charcoal_grey)
    val DarkIndigo @Composable get() = colorResource(id = R.color.dark_indigo)
    val Edittext @Composable get() = colorResource(id = R.color.edittext)
    val WarmGrey @Composable get() = colorResource(id = R.color.warm_grey)
    val Greyish @Composable get() = colorResource(id = R.color.greyish)
    val Error @Composable get() = colorResource(id = R.color.neon_red)
    val SteelGrey @Composable get() = colorResource(id = R.color.steel_grey)
}