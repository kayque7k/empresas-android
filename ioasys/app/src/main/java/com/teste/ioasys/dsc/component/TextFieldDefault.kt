package com.teste.ioasys.dsc.component

import androidx.annotation.DrawableRes
import androidx.compose.foundation.Image
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.size
import androidx.compose.foundation.text.KeyboardActions
import androidx.compose.foundation.text.KeyboardOptions
import androidx.compose.material.TextFieldColors
import androidx.compose.material.TextField
import androidx.compose.material.Text
import androidx.compose.material.IconButton
import androidx.compose.material.Icon
import androidx.compose.material.TextFieldDefaults
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.Close
import androidx.compose.material.icons.filled.Visibility
import androidx.compose.material.icons.filled.VisibilityOff
import androidx.compose.runtime.Composable
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.saveable.rememberSaveable
import androidx.compose.ui.Modifier
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.text.input.KeyboardType
import androidx.compose.ui.text.input.PasswordVisualTransformation
import androidx.compose.ui.text.input.TextFieldValue
import androidx.compose.ui.text.input.VisualTransformation
import androidx.compose.ui.unit.Dp
import com.teste.ioasys.dsc.color.ColorPalette
import com.teste.ioasys.dsc.dimen.Font
import com.teste.ioasys.dsc.dimen.Size
import com.teste.ioasys.R

@Composable
fun DefaultTextField(
    value: TextFieldValue,
    onValueChange: (TextFieldValue) -> Unit,
    onClickClose: () -> Unit = {},
    modifier: Modifier = Modifier,
    iconSize: Dp = Size.Size26_5,
    label: String,
    @DrawableRes icon: Int,
    enabled: Boolean = true,
    isError: Boolean = false,
    isClose: Boolean = false,
    keyboardOptions: KeyboardOptions = KeyboardOptions.Default,
    keyboardActions: KeyboardActions = KeyboardActions(),
    colors: TextFieldColors = ColorDark()
) {
    val passwordVisible = rememberSaveable { mutableStateOf(false) }
    val isPassword = keyboardOptions.keyboardType == KeyboardType.Password
    Column(modifier = modifier.fillMaxWidth()) {
        TextField(
            modifier = Modifier
                .fillMaxWidth(),
            label = {
                Text(
                    label,
                    color = ColorPalette.CharcoalGrey,
                    fontSize = Font.Font17
                )
            },
            leadingIcon = {
                Image(
                    modifier = Modifier.size(iconSize),
                    painter = painterResource(id = icon),
                    contentDescription = null
                )
            },
            trailingIcon = {
                if (isPassword) {
                    val image = if (passwordVisible.value)
                        Icons.Filled.Visibility
                    else Icons.Filled.VisibilityOff

                    IconButton(onClick = { passwordVisible.value = !passwordVisible.value }) {
                        Icon(
                            imageVector = image,
                            contentDescription = if (passwordVisible.value)
                                stringResource(id = R.string.password_hide)
                            else
                                stringResource(id = R.string.password_show)
                        )
                    }
                } else if (isClose) {
                    IconButton(onClick = onClickClose) {
                        Icon(
                            imageVector = Icons.Filled.Close,
                            tint = ColorPalette.White,
                            contentDescription = stringResource(id = R.string.close)
                        )
                    }
                }
            },
            value = value,
            enabled = enabled,
            onValueChange = onValueChange,
            isError = isError,
            keyboardOptions = keyboardOptions,
            keyboardActions = keyboardActions,
            singleLine = true,
            visualTransformation = if (isPassword && !passwordVisible.value) PasswordVisualTransformation() else VisualTransformation.None,
            colors = colors
        )
    }
}

@Composable
fun ColorDark() = TextFieldDefaults.textFieldColors(
    backgroundColor = ColorPalette.Transparent,
    textColor = ColorPalette.Edittext,
    disabledTextColor = ColorPalette.Edittext,
    focusedIndicatorColor = ColorPalette.Edittext,
    unfocusedIndicatorColor = ColorPalette.Edittext,
    disabledIndicatorColor = ColorPalette.Edittext,
    focusedLabelColor = ColorPalette.Edittext,
    unfocusedLabelColor = ColorPalette.Edittext,
    cursorColor = ColorPalette.Edittext,
    errorCursorColor = ColorPalette.Error,
    errorIndicatorColor = ColorPalette.Error,
    errorLabelColor = ColorPalette.Error,
    errorTrailingIconColor = ColorPalette.Error
)

@Composable
fun ColorWhite() = TextFieldDefaults.textFieldColors(
    backgroundColor = ColorPalette.Transparent,
    textColor = ColorPalette.White,
    disabledTextColor = ColorPalette.Edittext,
    focusedIndicatorColor = ColorPalette.White,
    unfocusedIndicatorColor = ColorPalette.White,
    disabledIndicatorColor = ColorPalette.White,
    focusedLabelColor = ColorPalette.White,
    unfocusedLabelColor = ColorPalette.White,
    cursorColor = ColorPalette.White,
    errorCursorColor = ColorPalette.Error,
    errorIndicatorColor = ColorPalette.Error,
    errorLabelColor = ColorPalette.Error,
    errorTrailingIconColor = ColorPalette.Error
)
