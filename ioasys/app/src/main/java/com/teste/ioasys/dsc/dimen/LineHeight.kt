package com.teste.ioasys.dsc.dimen

import androidx.compose.ui.unit.sp

object LineHeight {
    val Height19_5 = 19.5.sp
}