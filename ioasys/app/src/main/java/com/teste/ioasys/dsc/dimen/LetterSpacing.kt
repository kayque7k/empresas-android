package com.teste.ioasys.dsc.dimen

import androidx.compose.ui.unit.sp

object LetterSpacing {
    val LetterSpacingSub14 = -0.14.sp
    val LetterSpacingSub23 = -0.23.sp
    val LetterSpacingSub27 = -0.27.sp
}