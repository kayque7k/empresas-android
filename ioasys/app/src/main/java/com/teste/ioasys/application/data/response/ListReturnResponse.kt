package com.teste.ioasys.application.data.response

data class ListReturnResponse(
    val enterprises: List<EnterpriseResponse>
) {
    companion object {
        fun mock() = ListReturnResponse(
            enterprises = EnterpriseResponse.mocks()
        )
    }
}
