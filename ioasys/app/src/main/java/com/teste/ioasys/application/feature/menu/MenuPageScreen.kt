package com.teste.ioasys.application.feature.menu

import androidx.compose.animation.AnimatedVisibility
import androidx.compose.foundation.Image
import androidx.compose.foundation.layout.systemBarsPadding
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.size
import androidx.compose.foundation.rememberScrollState
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.foundation.verticalScroll
import androidx.compose.material.MaterialTheme
import androidx.compose.material.Surface
import androidx.compose.material.Text
import androidx.compose.material.TopAppBar
import androidx.compose.material.Card
import androidx.compose.material.IconButton
import androidx.compose.material.Icon
import androidx.compose.material.CircularProgressIndicator
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.Search
import androidx.compose.runtime.Composable
import androidx.compose.runtime.collectAsState
import androidx.compose.runtime.LaunchedEffect
import androidx.compose.runtime.getValue
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.layout.ContentScale
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.text.font.FontStyle
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.text.style.TextAlign
import coil.compose.rememberAsyncImagePainter
import com.google.accompanist.insets.ProvideWindowInsets
import com.teste.ioasys.R
import com.teste.ioasys.application.feature.main.IossysActivity
import com.teste.ioasys.application.feature.main.IossysViewModel
import com.teste.ioasys.dsc.dimen.Size
import com.teste.ioasys.application.feature.menu.MenuViewModel.ScreenEvent
import com.teste.ioasys.application.feature.menu.MenuViewModel.UiTextField
import com.teste.ioasys.application.feature.menu.MenuViewModel.UiState
import com.teste.ioasys.coreapi.BASE
import com.teste.ioasys.dsc.color.ColorPalette
import com.teste.ioasys.dsc.component.ColorWhite
import com.teste.ioasys.dsc.component.DefaultTextField
import com.teste.ioasys.dsc.component.rippleClickable
import com.teste.ioasys.dsc.dimen.Font
import com.teste.ioasys.dsc.dimen.LetterSpacing
import com.teste.ioasys.dsc.dimen.Radius

@Composable
fun MenuPageScreen(
    viewModel: MenuViewModel,
    flowViewModel: IossysViewModel
) {
    val activity = LocalContext.current as IossysActivity

    Screen(
        uiState = viewModel.uiState,
        onValueChange = viewModel::onValueChange,
        onClickSearch = viewModel::onClickSearch,
        onClickItem = {
            flowViewModel.setIdEnterprise(it)
            viewModel.onClickItem()
        }
    )

    EventConsumer(
        activity = activity,
        viewModel = viewModel,
        flowviewModel = flowViewModel
    )
}

@Composable
private fun Screen(
    uiState: UiState,
    onValueChange: (UiTextField) -> Unit,
    onClickSearch: () -> Unit,
    onClickItem: (Int) -> Unit
) = MaterialTheme {
    ProvideWindowInsets {
        Surface(
            modifier = Modifier.fillMaxSize(),
            color = ColorPalette.MediumPink,
        ) {
            Column(
                modifier = Modifier.systemBarsPadding()
            ) {
                SearchBar(
                    uiState = uiState,
                    onValueChange = onValueChange,
                    onClickSearch = onClickSearch
                )
                Surface(
                    modifier = Modifier.fillMaxSize(),
                    color = ColorPalette.Beige,
                ) {
                    val screenState by uiState.screenState.collectAsState()
                    when (screenState) {
                        is MenuViewModel.ScreenState.ScreenLoading -> Column(
                            modifier = Modifier
                                .fillMaxSize(),
                            verticalArrangement = Arrangement.Center,
                            horizontalAlignment = Alignment.CenterHorizontally
                        ) { ScreenProgress() }
                        is MenuViewModel.ScreenState.ScreenContent ->
                            if (!uiState.search.collectAsState().value) {
                                Box(
                                    modifier = Modifier.fillMaxSize(),
                                    contentAlignment = Alignment.Center
                                ) {
                                    Text(
                                        text = stringResource(id = R.string.click_init),
                                        color = ColorPalette.CharcoalGrey,
                                        fontSize = Font.Font16,
                                        letterSpacing = LetterSpacing.LetterSpacingSub23,
                                        textAlign = TextAlign.Center
                                    )
                                }
                            } else {
                                Column(
                                    modifier = Modifier
                                        .fillMaxWidth()
                                        .verticalScroll(rememberScrollState())
                                ) {
                                    List(
                                        uiState = uiState,
                                        onClickItem = onClickItem
                                    )
                                }
                            }
                        is MenuViewModel.ScreenState.ScreenError -> Column(
                            modifier = Modifier
                                .fillMaxSize()
                        ) {
                            Text(
                                modifier = Modifier.padding(
                                    all = Size.Size64,
                                ),
                                text = stringResource(id = R.string.not_search),
                                color = ColorPalette.Greyish,
                                fontSize = Font.Font17,
                                textAlign = TextAlign.Center
                            )
                        }
                    }
                }
            }
        }
    }
}

@Composable
fun SearchBar(
    uiState: UiState,
    onValueChange: (UiTextField) -> Unit,
    onClickSearch: () -> Unit
) = TopAppBar(
    backgroundColor = ColorPalette.MediumPink,
    title = {
        AnimatedVisibility(
            modifier = Modifier.fillMaxWidth(),
            visible = !uiState.search.collectAsState().value
        ) {
            Image(
                modifier = Modifier.fillMaxWidth(),
                painter = painterResource(id = R.drawable.logo_nav),
                contentDescription = stringResource(id = R.string.logo),
            )
        }
        AnimatedVisibility(visible = uiState.search.collectAsState().value) {
            DefaultTextField(
                modifier = Modifier
                    .fillMaxWidth()
                    .padding(horizontal = Size.Size8),
                value = uiState.tfSearch.collectAsState().value.value,
                onValueChange = {
                    onValueChange(UiTextField.Search(it))
                },
                label = stringResource(id = R.string.search),
                icon = R.drawable.ic_search,
                iconSize = Size.Size30,
                isClose = true,
                onClickClose = onClickSearch,
                colors = ColorWhite()
            )
        }
    },
    actions = {
        AnimatedVisibility(visible = !uiState.search.collectAsState().value) {
            IconButton(onClick = onClickSearch) {
                Icon(
                    Icons.Filled.Search,
                    null,
                    tint = ColorPalette.White
                )
            }
        }
    })

@Composable
fun List(
    uiState: UiState,
    onClickItem: (Int) -> Unit
) = Column {
    uiState.listEnterprise.collectAsState().value.forEach {
        Card(
            modifier = Modifier
                .fillMaxWidth()
                .padding(
                    start = Size.Size16,
                    end = Size.Size16,
                    top = Size.Size16,
                )
                .rippleClickable {
                    onClickItem(it.id)
                },
            shape = RoundedCornerShape(Radius.Radius2_4),
            backgroundColor = ColorPalette.White
        ) {
            Row {
                Image(
                    modifier = Modifier
                        .size(Size.Size100)
                        .padding(
                            top = Size.Size16,
                            bottom = Size.Size16,
                            start = Size.Size16
                        ),
                    contentScale = ContentScale.Crop,
                    painter = rememberAsyncImagePainter("$BASE/${it.photo}"),
                    contentDescription = stringResource(id = R.string.image)
                )
                Column(
                    modifier = Modifier.padding(all = Size.Size16)
                ) {
                    Text(
                        modifier = Modifier.padding(bottom = Size.Size4),
                        text = it.name,
                        color = ColorPalette.DarkIndigo,
                        fontWeight = FontWeight.Bold,
                        fontSize = Font.Font17,
                        textAlign = TextAlign.Left
                    )
                    Text(
                        modifier = Modifier.padding(bottom = Size.Size2),
                        text = it.type.typeName,
                        color = ColorPalette.WarmGrey,
                        fontStyle = FontStyle.Italic,
                        fontSize = Font.Font17,
                        textAlign = TextAlign.Left
                    )
                    Text(
                        text = it.country,
                        color = ColorPalette.WarmGrey,
                        fontSize = Font.Font17,
                        textAlign = TextAlign.Left
                    )
                }
            }
        }
    }
}

@Composable
private fun ScreenProgress() {
    Box(
        modifier = Modifier.fillMaxSize(),
        contentAlignment = Alignment.Center
    ) {
        CircularProgressIndicator(
            modifier = Modifier.size(Size.SizeProgress),
            color = ColorPalette.GreenyBlue
        )
    }
}

@Composable
private fun EventConsumer(
    activity: IossysActivity,
    viewModel: MenuViewModel,
    flowviewModel: IossysViewModel
) {
    LaunchedEffect(key1 = viewModel) {
        viewModel.eventsFlow.collect { event ->
            when (event) {
                ScreenEvent.GoBack -> activity.onBackPressed()
                is ScreenEvent.NavigateTo -> flowviewModel.navigate(event.navigation)
            }
        }
    }
}
