package com.teste.ioasys.application.feature.main

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.NavGraphBuilder
import androidx.navigation.NavHostController
import androidx.navigation.compose.composable
import com.teste.ioasys.R
import com.teste.ioasys.application.feature.detail.DetailPageScreen
import com.teste.ioasys.application.feature.detail.DetailViewModel
import com.teste.ioasys.commons.extensions.setBackgroundColor
import com.teste.ioasys.commons.extensions.setStatusBarColor
import com.teste.ioasys.commons.navigartion.composeNavigate
import com.teste.ioasys.commons.navigartion.setNavigationContent
import com.teste.ioasys.application.feature.login.LoginPageScreen
import com.teste.ioasys.application.feature.login.LoginViewModel
import com.teste.ioasys.application.feature.main.IossysViewModel.Navigation.Login
import com.teste.ioasys.application.feature.main.IossysViewModel.Navigation.Detail
import com.teste.ioasys.application.feature.main.IossysViewModel.Navigation.Menu
import com.teste.ioasys.application.feature.main.IossysViewModel.Navigation
import com.teste.ioasys.application.feature.menu.MenuPageScreen
import com.teste.ioasys.application.feature.menu.MenuViewModel

class IossysActivity : AppCompatActivity() {

    private lateinit var _flowViewModel: IossysViewModel

    private lateinit var _loginViewModel: LoginViewModel

    private lateinit var _menuViewModel: MenuViewModel

    private lateinit var _detailViewModel: DetailViewModel

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        startViewModel()
        setStatusBarColor(R.color.black)
        setBackgroundColor(R.color.beige)

        _flowViewModel.run {
            setNavigationContent(
                startDestination = startDestination().route,
                navGraphBuilder = ::navGraphBuilder,
                navEventFlow = eventsFlow,
                navEvent = ::navEvent
            )
        }
    }

    private fun startViewModel() {
        _flowViewModel = ViewModelProvider(this).get(IossysViewModel::class.java)
        _loginViewModel = ViewModelProvider(this).get(LoginViewModel::class.java)
        _menuViewModel = ViewModelProvider(this).get(MenuViewModel::class.java)
        _detailViewModel = ViewModelProvider(this).get(DetailViewModel::class.java)
    }

    private fun navGraphBuilder(builder: NavGraphBuilder) = builder.apply {
        composable(Login.route) {
            LoginPageScreen(
                viewModel = _loginViewModel,
                flowViewModel = _flowViewModel
            )
        }
        composable(Menu.route) {
            MenuPageScreen(
                viewModel = _menuViewModel,
                flowViewModel = _flowViewModel
            )
        }
        composable(Detail.route) {
            DetailPageScreen(
                viewModel = _detailViewModel,
                flowViewModel = _flowViewModel
            )
        }
    }

    private fun navEvent(navController: NavHostController, navScreen: Navigation) {
        navController.composeNavigate(
            route = navScreen.route,
            popStack = navScreen.popStack
        )
    }
}