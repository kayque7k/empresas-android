package com.teste.ioasys.application.data.api

import com.teste.ioasys.application.data.request.LoginRequest
import retrofit2.Response
import retrofit2.http.Body
import retrofit2.http.POST

interface LoginApi {

    @POST("users/auth/sign_in")
    suspend fun getLogin(@Body request: LoginRequest) : Response<Unit>
}