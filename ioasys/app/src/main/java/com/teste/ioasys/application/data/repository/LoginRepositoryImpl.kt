package com.teste.ioasys.application.data.repository

import com.teste.ioasys.application.data.api.LoginApi
import com.teste.ioasys.application.data.request.LoginRequest
import com.teste.ioasys.application.domain.repository.LoginRepository
import com.teste.ioasys.commons.extensions.EMPTY_STRING
import com.teste.ioasys.coreapi.AuthInterceptor
import com.teste.ioasys.coreapi.SUCCESS_CODE
import com.teste.ioasys.coreapi.Session
import com.teste.ioasys.coreapi.ACCESS_TOKEN
import com.teste.ioasys.coreapi.CLIENT
import com.teste.ioasys.coreapi.UID

class LoginRepositoryImpl(
    val loginApi: LoginApi = AuthInterceptor.retrofit().create(LoginApi::class.java)
) : LoginRepository {

    override suspend fun login(
        email: String,
        password: String
    ) {
        val response = loginApi.getLogin(
            request = LoginRequest(
                email = email,
                password = password
            )
        )

        if(response.code() != SUCCESS_CODE) throw Exception()

        response.headers().run {
            Session.setToken(
                accesstoken = get(ACCESS_TOKEN) ?: EMPTY_STRING,
                client = get(CLIENT) ?: EMPTY_STRING,
                uid = get(UID) ?: EMPTY_STRING,
            )
        }


    }
}