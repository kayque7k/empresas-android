package com.teste.ioasys.application.feature.main

import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.teste.ioasys.commons.extensions.EMPTY_STRING
import com.teste.ioasys.commons.viewModel.ChannelEventSenderImpl
import com.teste.ioasys.commons.viewModel.EventSender
import com.teste.ioasys.application.feature.main.IossysViewModel.Navigation

class IossysViewModel : ViewModel(),
    EventSender<Navigation> by ChannelEventSenderImpl() {

    var idEnterprise: Int = ZERO
        private set

    fun setIdEnterprise(id: Int) {
        this.idEnterprise = id
    }

    fun startDestination() = Navigation.Login

    fun navigate(event: Navigation) = viewModelScope.sendEvent(event = event)

    sealed class Navigation(
        val route: String = EMPTY_STRING,
        val popStack: Boolean = false
    ) {
        object Login : Navigation(route = "login")
        object Menu : Navigation(route = "menu")
        object Detail : Navigation(route = "detail")
    }

    companion object {
        private const val ZERO: Int = 0
    }
}