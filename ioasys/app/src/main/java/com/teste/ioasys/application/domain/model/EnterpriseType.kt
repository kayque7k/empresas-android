package com.teste.ioasys.application.domain.model

import com.teste.ioasys.commons.extensions.EMPTY_STRING

data class EnterpriseType(
    val id: Int = 0,
    val typeName: String = EMPTY_STRING
) {
    companion object {
        fun mock() = EnterpriseType(
            id = 1,
            typeName = "top"
        )
    }
}
