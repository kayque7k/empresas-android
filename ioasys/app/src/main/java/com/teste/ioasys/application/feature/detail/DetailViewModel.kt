package com.teste.ioasys.application.feature.detail

import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.teste.ioasys.application.domain.model.Enterprise
import com.teste.ioasys.commons.viewModel.ChannelEventSenderImpl
import com.teste.ioasys.commons.viewModel.EventSender
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.launch
import com.teste.ioasys.application.feature.detail.DetailViewModel.ScreenEvent
import com.teste.ioasys.application.feature.main.IossysViewModel.Navigation
import com.teste.ioasys.application.usecase.EnterpriseDetailsUseCase
import com.teste.ioasys.commons.extensions.Result

class DetailViewModel : ViewModel(), EventSender<ScreenEvent> by ChannelEventSenderImpl() {

    val detailsUseCase = EnterpriseDetailsUseCase()

    val uiState = UiState()

    fun onClickBack() = viewModelScope.launch {
        sendEvent(event = ScreenEvent.GoBack)
    }

    fun getDetails(id: Int) = viewModelScope.launch {
        uiState.run {
            screenState.value = ScreenState.ScreenLoading
            when (val result = detailsUseCase.execute(
                id = id
            )) {
                is Result.Success -> {
                    enterprise.value = result.data
                    screenState.value = ScreenState.ScreenContent
                }
                is Result.Failure -> {
                    screenState.value = ScreenState.ScreenError
                }
            }
        }
    }

    sealed class ScreenState {
        object ScreenLoading : ScreenState()
        object ScreenContent : ScreenState()
        object ScreenError : ScreenState()
    }

    sealed class ScreenEvent {
        object GoBack : ScreenEvent()
        data class NavigateTo(val navigation: Navigation) : ScreenEvent()
    }

    class UiState {
        val screenState = MutableStateFlow<ScreenState>(ScreenState.ScreenContent)

        val enterprise = MutableStateFlow(Enterprise())
    }

}
