package com.teste.ioasys.application.feature.detail

import androidx.compose.foundation.Image
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.systemBarsPadding
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.size
import androidx.compose.foundation.layout.Box
import androidx.compose.material.MaterialTheme
import androidx.compose.material.Surface
import androidx.compose.material.Text
import androidx.compose.material.TopAppBar
import androidx.compose.material.IconButton
import androidx.compose.material.CircularProgressIndicator
import androidx.compose.material.Icon
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.ArrowBack
import androidx.compose.runtime.Composable
import androidx.compose.runtime.LaunchedEffect
import androidx.compose.runtime.collectAsState
import androidx.compose.runtime.getValue
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.layout.ContentScale
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.text.style.TextAlign
import coil.compose.rememberAsyncImagePainter
import com.google.accompanist.insets.ProvideWindowInsets
import com.teste.ioasys.R
import com.teste.ioasys.application.feature.main.IossysActivity
import com.teste.ioasys.application.feature.main.IossysViewModel
import com.teste.ioasys.dsc.dimen.Size
import com.teste.ioasys.application.feature.detail.DetailViewModel.UiState
import com.teste.ioasys.coreapi.BASE
import com.teste.ioasys.dsc.color.ColorPalette
import com.teste.ioasys.dsc.dimen.Font
import com.teste.ioasys.dsc.dimen.LetterSpacing

@Composable
fun DetailPageScreen(
    viewModel: DetailViewModel,
    flowViewModel: IossysViewModel
) {
    val activity = LocalContext.current as IossysActivity

    LaunchedEffect(viewModel) {
        viewModel.getDetails(
            id = flowViewModel.idEnterprise
        )
    }

    Screen(
        uiState = viewModel.uiState,
        onClickClose = viewModel::onClickBack
    )

    EventConsumer(
        activity = activity,
        viewModel = viewModel,
        flowviewModel = flowViewModel
    )
}

@Composable
private fun Screen(
    uiState: UiState,
    onClickClose: () -> Unit
) = MaterialTheme {
    ProvideWindowInsets {
        Surface(
            modifier = Modifier.fillMaxSize(),
            color = ColorPalette.MediumPink,
        ) {
            Column(
                modifier = Modifier.systemBarsPadding()
            ) {
                ToolBar(
                    uiState = uiState,
                    onClickClose = onClickClose,
                )
                Surface(
                    modifier = Modifier.fillMaxSize(),
                    color = ColorPalette.Beige,
                ) {
                    val screenState by uiState.screenState.collectAsState()
                    when (screenState) {
                        is DetailViewModel.ScreenState.ScreenLoading -> Column(
                            modifier = Modifier
                                .fillMaxSize(),
                            verticalArrangement = Arrangement.Center,
                            horizontalAlignment = Alignment.CenterHorizontally
                        ) { ScreenProgress() }
                        is DetailViewModel.ScreenState.ScreenContent -> {
                            Column {
                                Image(
                                    modifier = Modifier
                                        .fillMaxWidth()
                                        .height(Size.Size256)
                                        .padding(all = Size.Size16),
                                    contentScale = ContentScale.Fit,
                                    painter = rememberAsyncImagePainter(
                                        "$BASE/${
                                            uiState.enterprise.collectAsState().value.photo
                                        }"
                                    ),
                                    contentDescription = stringResource(id = R.string.image)
                                )
                                Text(
                                    modifier = Modifier.padding(
                                        start = Size.Size32,
                                        end = Size.Size32,
                                        bottom = Size.Size32,
                                    ),
                                    text = uiState.enterprise.collectAsState().value.description,
                                    fontSize = Font.Font17,
                                    letterSpacing = LetterSpacing.LetterSpacingSub27,
                                    color = ColorPalette.WarmGrey,
                                    textAlign = TextAlign.Left
                                )
                            }
                        }
                        is DetailViewModel.ScreenState.ScreenError -> Column(
                            modifier = Modifier
                                .fillMaxSize()
                        ) {
                            Text(
                                modifier = Modifier.padding(
                                    all = Size.Size64,
                                ),
                                text = stringResource(id = R.string.not_search),
                                color = ColorPalette.Greyish,
                                fontSize = Font.Font17,
                                textAlign = TextAlign.Center
                            )
                        }
                    }
                }
            }
        }
    }
}

@Composable
fun ToolBar(
    uiState: UiState,
    onClickClose: () -> Unit
) = TopAppBar(
    backgroundColor = ColorPalette.MediumPink,
    title = {
        Text(
            text = uiState.enterprise.collectAsState().value.name,
            color = ColorPalette.White,
            fontSize = Font.Font17
        )
    },
    navigationIcon = {
        IconButton(onClick = onClickClose) {
            Icon(
                Icons.Filled.ArrowBack,
                null,
                tint = ColorPalette.White
            )
        }
    })

@Composable
private fun ScreenProgress() {
    Box(
        modifier = Modifier.fillMaxSize(),
        contentAlignment = Alignment.Center
    ) {
        CircularProgressIndicator(
            modifier = Modifier.size(Size.SizeProgress),
            color = ColorPalette.GreenyBlue
        )
    }
}

@Composable
private fun EventConsumer(
    activity: IossysActivity,
    viewModel: DetailViewModel,
    flowviewModel: IossysViewModel
) {
    LaunchedEffect(key1 = viewModel) {
        viewModel.eventsFlow.collect { event ->
            when (event) {
                DetailViewModel.ScreenEvent.GoBack -> activity.onBackPressed()
                is DetailViewModel.ScreenEvent.NavigateTo -> flowviewModel.navigate(event.navigation)
            }
        }
    }
}
