package com.teste.ioasys.dsc.dimen

import androidx.compose.ui.unit.sp

object Font {

    val Font10 = 10.sp
    val Font15 = 15.sp
    val Font16 = 16.sp
    val Font17 = 17.sp
    val Font20 = 20.sp
}