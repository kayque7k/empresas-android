package com.teste.ioasys.application.data.api

import com.teste.ioasys.application.data.response.ListReturnResponse
import com.teste.ioasys.application.data.response.ReturnResponse
import retrofit2.http.GET
import retrofit2.http.Path
import retrofit2.http.Query

interface EnterprisesApi {

    @GET("enterprises")
    suspend fun getEnterprises(
        @Query("name") name: String
    ): ListReturnResponse

    @GET("enterprises/{id}")
    suspend fun getEnterprise(@Path("id") id: Int): ReturnResponse
}