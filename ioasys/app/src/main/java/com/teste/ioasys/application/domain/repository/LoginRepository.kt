package com.teste.ioasys.application.domain.repository

interface LoginRepository {

    suspend fun login(
        email: String,
        password: String
    )

}