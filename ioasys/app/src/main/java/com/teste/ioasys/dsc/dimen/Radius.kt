package com.teste.ioasys.dsc.dimen

import androidx.compose.runtime.Composable
import androidx.compose.ui.res.dimensionResource
import androidx.compose.ui.unit.Dp
import com.teste.ioasys.R

object Radius {
    val Radius2_4: Dp @Composable get() = dimensionResource(id = R.dimen.radius_2_4)
}