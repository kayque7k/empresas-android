package com.teste.ioasys.application.data.repository

import com.teste.ioasys.application.data.api.EnterprisesApi
import com.teste.ioasys.coreapi.AuthInterceptor
import com.teste.ioasys.application.data.mapper.EnterpriseMapper.toEnterprise
import com.teste.ioasys.application.data.mapper.EnterpriseMapper.toEnterpriseList
import com.teste.ioasys.application.domain.repository.EnterprisesRepository

class EnterprisesRepositoryImpl(
    val enterprisesApi: EnterprisesApi = AuthInterceptor.retrofit().create(EnterprisesApi::class.java)
) : EnterprisesRepository {

    override suspend fun menu(
        name: String
    ) = enterprisesApi.getEnterprises(
        name = name
    ).enterprises.toEnterpriseList()

    override suspend fun detail(
        id: Int
    ) = enterprisesApi.getEnterprise(
        id = id
    ).enterprise.toEnterprise()
}