package com.teste.ioasys.application.data.response

data class ReturnResponse(
    val enterprise: EnterpriseResponse
) {
    companion object {
        fun mock() = ReturnResponse(
            enterprise = EnterpriseResponse.mock()
        )
    }
}
