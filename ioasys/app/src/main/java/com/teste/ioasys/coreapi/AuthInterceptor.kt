package com.teste.ioasys.coreapi

import okhttp3.OkHttpClient
import okhttp3.Request
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

const val BASE: String = "https://empresas.ioasys.com.br"
const val V1: String = "v1"
const val END_POINT: String = "$BASE/api/$V1/"

const val ACCESS_TOKEN: String = "access-token"
const val CLIENT: String = "client"
const val UID: String = "uid"
const val SUCCESS_CODE: Int = 200

object AuthInterceptor {

    fun retrofit() = Retrofit.Builder()
        .baseUrl(END_POINT)
        .addConverterFactory(GsonConverterFactory.create())
        .client(headers())
        .build()

    private fun headers(): OkHttpClient {
        val httpClient = OkHttpClient.Builder()

        httpClient.addInterceptor { chain ->
            Session
            val request: Request =
                chain.request().newBuilder()
                    .addHeader(ACCESS_TOKEN, Session.accesstoken)
                    .addHeader(CLIENT, Session.client)
                    .addHeader(UID, Session.uid)
                    .build()
            chain.proceed(request)
        }

        return httpClient.build()
    }
}