package com.teste.ioasys.application.feature.login

import androidx.compose.animation.AnimatedVisibility
import androidx.compose.foundation.Image
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.size
import androidx.compose.foundation.rememberScrollState
import androidx.compose.foundation.text.KeyboardOptions
import androidx.compose.foundation.verticalScroll
import androidx.compose.material.MaterialTheme
import androidx.compose.material.Text
import androidx.compose.material.Button
import androidx.compose.material.ButtonDefaults
import androidx.compose.material.CircularProgressIndicator
import androidx.compose.runtime.Composable
import androidx.compose.runtime.LaunchedEffect
import androidx.compose.runtime.collectAsState
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.text.input.KeyboardType
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.tooling.preview.Preview
import com.teste.ioasys.dsc.color.ColorPalette
import com.teste.ioasys.dsc.dimen.Size
import com.teste.ioasys.application.feature.main.IossysViewModel
import com.teste.ioasys.application.feature.login.LoginViewModel.ScreenEvent
import com.teste.ioasys.application.feature.login.LoginViewModel.UiState
import com.teste.ioasys.application.feature.login.LoginViewModel.UiTextField
import com.teste.ioasys.application.feature.main.IossysActivity
import com.teste.ioasys.R
import com.teste.ioasys.dsc.component.DefaultTextField
import com.teste.ioasys.dsc.component.SpacerVertical
import com.teste.ioasys.dsc.dimen.Font
import com.teste.ioasys.dsc.dimen.LetterSpacing
import com.teste.ioasys.dsc.dimen.LineHeight

@Composable
fun LoginPageScreen(
    viewModel: LoginViewModel,
    flowViewModel: IossysViewModel
) {
    val activity = LocalContext.current as IossysActivity

    Screen(
        uiState = viewModel.uiState,
        onValueChange = viewModel::onValueChange,
        onClickButton = viewModel::onClickLogin
    )

    EventConsumer(
        activity = activity,
        viewModel = viewModel,
        flowviewModel = flowViewModel
    )
}

@Composable
private fun Screen(
    uiState: UiState,
    onValueChange: (UiTextField) -> Unit,
    onClickButton: () -> Unit
) = MaterialTheme {
    Box {
        AnimatedVisibility(visible = uiState.loading.collectAsState().value) {
            ScreenProgress()
        }
        Body(
            uiState = uiState,
            onValueChange = onValueChange,
            onClickButton = onClickButton
        )
    }
}

@Composable
private fun Body(
    uiState: UiState,
    onValueChange: (UiTextField) -> Unit,
    onClickButton: () -> Unit
) {
    Column(
        modifier = Modifier
            .fillMaxSize()
            .verticalScroll(rememberScrollState()),
        horizontalAlignment = Alignment.CenterHorizontally
    ) {
        SpacerVertical(Size.Size64)
        Image(
            modifier = Modifier.padding(horizontal = Size.Size64),
            painter = painterResource(id = R.drawable.logo_home),
            contentDescription = stringResource(id = R.string.logo)
        )
        SpacerVertical(Size.Size64)
        Text(
            modifier = Modifier.padding(horizontal = Size.Size128),
            text = stringResource(id = R.string.welcome),
            fontWeight = FontWeight.Bold,
            fontSize = Font.Font20,
            textAlign = TextAlign.Center
        )
        SpacerVertical(Size.Size16)
        Text(
            modifier = Modifier.padding(horizontal = Size.Size64),
            text = stringResource(id = R.string.lorem_ipsum),
            fontSize = Font.Font16,
            letterSpacing = LetterSpacing.LetterSpacingSub23,
            textAlign = TextAlign.Center
        )
        SpacerVertical(Size.Size64)
        DefaultTextField(
            modifier = Modifier
                .fillMaxWidth()
                .padding(horizontal = Size.Size32),
            value = uiState.tfEmail.collectAsState().value.value,
            onValueChange = {
                onValueChange(UiTextField.Email(it))
            },
            label = stringResource(id = R.string.e_mail),
            icon = R.drawable.ic_email,
            isError = uiState.error.collectAsState().value
        )
        SpacerVertical(Size.Size16)
        DefaultTextField(
            modifier = Modifier
                .fillMaxWidth()
                .padding(horizontal = Size.Size32),
            value = uiState.tfPassword.collectAsState().value.value,
            onValueChange = {
                onValueChange(UiTextField.Password(it))
            },
            label = stringResource(id = R.string.password),
            keyboardOptions = KeyboardOptions(keyboardType = KeyboardType.Password),
            icon = R.drawable.ic_cadeado,
            isError = uiState.error.collectAsState().value
        )
        AnimatedVisibility(visible = uiState.error.collectAsState().value) {
            Column(
                modifier = Modifier
                    .fillMaxSize(),
                horizontalAlignment = Alignment.CenterHorizontally
            ) {
                SpacerVertical(Size.Size8)
                Text(
                    modifier = Modifier.padding(horizontal = Size.Size32),
                    text = stringResource(id = R.string.error),
                    fontSize = Font.Font10,
                    letterSpacing = LetterSpacing.LetterSpacingSub14,
                    textAlign = TextAlign.Center,
                    lineHeight = LineHeight.Height19_5,
                    color = ColorPalette.Error
                )
            }
        }
        SpacerVertical(Size.Size32)
        Button(
            modifier = Modifier
                .fillMaxWidth()
                .padding(horizontal = Size.Size32),
            onClick = onClickButton,
            colors = ButtonDefaults.textButtonColors(
                backgroundColor = uiState.bgButtom(),
            ),
            enabled = !uiState.error.collectAsState().value
        ) {
            Text(
                modifier = Modifier
                    .padding(vertical = Size.Size16),
                text = stringResource(id = R.string.enter),
                fontSize = Font.Font15,
                color = ColorPalette.White
            )
        }
        SpacerVertical(Size.Size128)
    }
}

@Composable
private fun ScreenProgress() {
    Box(
        modifier = Modifier.fillMaxSize(),
        contentAlignment = Alignment.Center
    ) {
        CircularProgressIndicator(
            modifier = Modifier.size(Size.SizeProgress),
            color = ColorPalette.GreenyBlue
        )
    }
}

@Composable
private fun EventConsumer(
    activity: IossysActivity,
    viewModel: LoginViewModel,
    flowviewModel: IossysViewModel
) {
    LaunchedEffect(key1 = viewModel) {
        viewModel.eventsFlow.collect { event ->
            when (event) {
                ScreenEvent.GoBack -> activity.onBackPressed()
                is ScreenEvent.NavigateTo -> flowviewModel.navigate(event.navigation)
            }
        }
    }
}

@Composable
@Preview
private fun ScreenPreview() = Screen(uiState = UiState(), onValueChange = {}, onClickButton = {})
