package com.teste.ioasys.application.feature.menu

import androidx.compose.ui.text.input.TextFieldValue
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.teste.ioasys.application.domain.model.Enterprise
import com.teste.ioasys.commons.viewModel.ChannelEventSenderImpl
import com.teste.ioasys.commons.viewModel.EventSender
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.launch
import com.teste.ioasys.application.feature.menu.MenuViewModel.ScreenEvent
import com.teste.ioasys.application.feature.main.IossysViewModel.Navigation
import com.teste.ioasys.application.usecase.EnterpriseMenuUseCase
import com.teste.ioasys.commons.extensions.Result

class MenuViewModel : ViewModel(), EventSender<ScreenEvent> by ChannelEventSenderImpl() {

    val menuUseCase = EnterpriseMenuUseCase()

    val uiState = UiState()

    fun onValueChange(value: UiTextField) = uiState.run {
        when (value) {
            is UiTextField.Search -> {
                tfSearch.value = value
                onSearch()
            }
        }
    }

    fun onClickItem() = viewModelScope.launch {
        sendEvent(ScreenEvent.NavigateTo(Navigation.Detail))
    }

    fun onClickSearch() = uiState.run {
        search.value = !search.value
    }

    fun onSearch() = viewModelScope.launch {
        uiState.run {
            screenState.value = ScreenState.ScreenLoading
            when (val result = menuUseCase.execute(
                name = tfSearch.value.value.text
            )) {
                is Result.Success -> {
                    listEnterprise.value = result.data

                    screenState.value = if (result.data.isEmpty())
                        ScreenState.ScreenError
                    else
                        ScreenState.ScreenContent
                }
                is Result.Failure -> {
                    screenState.value = ScreenState.ScreenError
                }
            }
        }
    }

    sealed class ScreenState {
        object ScreenLoading : ScreenState()
        object ScreenContent : ScreenState()
        object ScreenError : ScreenState()
    }

    sealed class ScreenEvent {
        object GoBack : ScreenEvent()
        data class NavigateTo(val navigation: Navigation) : ScreenEvent()
    }

    sealed class UiTextField(val value: TextFieldValue) {
        class Search(value: TextFieldValue) : UiTextField(value = value)
    }

    class UiState {
        val screenState = MutableStateFlow<ScreenState>(ScreenState.ScreenContent)

        val search = MutableStateFlow(false)

        val tfSearch = MutableStateFlow(UiTextField.Search(TextFieldValue()))
        val listEnterprise = MutableStateFlow(listOf<Enterprise>())
    }

}
