package com.teste.ioasys.application.feature.login

import androidx.compose.runtime.Composable
import androidx.compose.runtime.collectAsState
import androidx.compose.ui.text.input.TextFieldValue
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.teste.ioasys.commons.viewModel.ChannelEventSenderImpl
import com.teste.ioasys.commons.viewModel.EventSender
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.launch
import com.teste.ioasys.application.feature.login.LoginViewModel.ScreenEvent
import com.teste.ioasys.application.feature.main.IossysViewModel.Navigation
import com.teste.ioasys.application.usecase.LoginUseCase
import com.teste.ioasys.commons.extensions.Result
import com.teste.ioasys.dsc.color.ColorPalette

class LoginViewModel : ViewModel(), EventSender<ScreenEvent> by ChannelEventSenderImpl() {

    val loginUseCase = LoginUseCase()
    val uiState = UiState()

    fun onValueChange(value: UiTextField) = uiState.run {
        error.value = false
        when (value) {
            is UiTextField.Email -> tfEmail.value = value
            is UiTextField.Password -> tfPassword.value = value
        }
    }

    fun onClickLogin() = viewModelScope.launch {
        uiState.run {
            loading.value = true
            when (loginUseCase.execute(
                email = tfEmail.value.value.text,
                password = tfPassword.value.value.text
            )) {
                is Result.Success -> sendEvent(ScreenEvent.NavigateTo(navigation = Navigation.Menu))
                is Result.Failure -> error.value = true
            }
            loading.value = false
        }

    }

    sealed class ScreenEvent {
        object GoBack : ScreenEvent()
        data class NavigateTo(val navigation: Navigation) : ScreenEvent()
    }

    sealed class UiTextField(val value: TextFieldValue) {
        class Email(value: TextFieldValue) : UiTextField(value = value)
        class Password(value: TextFieldValue) : UiTextField(value = value)
    }

    class UiState {
        val loading = MutableStateFlow(false)
        val error = MutableStateFlow(false)

        val tfEmail = MutableStateFlow(UiTextField.Email(TextFieldValue()))
        val tfPassword = MutableStateFlow(UiTextField.Password(TextFieldValue()))

        @Composable
        fun bgButtom() = if (!error.collectAsState().value) ColorPalette.GreenyBlue else ColorPalette.SteelGrey
    }

}
