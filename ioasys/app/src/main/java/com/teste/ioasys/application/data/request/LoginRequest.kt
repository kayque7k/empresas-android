package com.teste.ioasys.application.data.request

import com.google.gson.annotations.SerializedName
import com.teste.ioasys.commons.extensions.EMPTY_STRING

data class LoginRequest                                                                  (
    @SerializedName("email") val email: String = EMPTY_STRING,
    @SerializedName("password") val password: String = EMPTY_STRING
)
