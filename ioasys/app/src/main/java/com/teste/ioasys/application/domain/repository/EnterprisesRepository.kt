package com.teste.ioasys.application.domain.repository

import com.teste.ioasys.application.domain.model.Enterprise

interface EnterprisesRepository {

    suspend fun menu(
        name: String
    ): List<Enterprise>

    suspend fun detail(
        id: Int
    ): Enterprise

}