package com.teste.ioasys.coreapi

import com.teste.ioasys.commons.extensions.EMPTY_STRING

object Session {

    private var _accesstoken: String = EMPTY_STRING
    private var _client: String = EMPTY_STRING
    private var _uid: String = EMPTY_STRING

    val accesstoken get() = _accesstoken
    val client get() = _client
    val uid get() = _uid

    fun setToken(
        accesstoken: String,
        client: String,
        uid: String
    ){
        this._accesstoken = accesstoken
        this._client = client
        this._uid = uid
    }
}