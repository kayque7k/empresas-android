package com.teste.ioasys.application.usecase

import com.teste.ioasys.application.data.repository.LoginRepositoryImpl
import com.teste.ioasys.application.domain.repository.LoginRepository
import com.teste.ioasys.commons.extensions.safeRunDispatcher

class LoginUseCase(
    val loginRepository: LoginRepository = LoginRepositoryImpl()
) {

    suspend fun execute(
        email: String,
        password: String
    ) = safeRunDispatcher {
        loginRepository.login(email = email, password = password)
    }
}