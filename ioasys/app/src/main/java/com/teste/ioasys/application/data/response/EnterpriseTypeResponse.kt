package com.teste.ioasys.application.data.response

import com.teste.ioasys.commons.extensions.EMPTY_STRING

data class EnterpriseTypeResponse(
    val id: Int = 0,
    val enterprise_type_name: String = EMPTY_STRING
) {
    companion object {
        fun mock() = EnterpriseTypeResponse(
            id = 1,
            enterprise_type_name = "top"
        )
    }
}
