package com.teste.ioasys.application.data.mapper

import com.teste.ioasys.application.data.response.EnterpriseResponse
import com.teste.ioasys.application.domain.model.Enterprise
import com.teste.ioasys.application.domain.model.EnterpriseType

object EnterpriseMapper {
    fun List<EnterpriseResponse>.toEnterpriseList() = map {
        it.toEnterprise()
    }

    fun EnterpriseResponse.toEnterprise() = Enterprise(
        id = id,
        name = enterprise_name,
        description = description,
        photo = photo,
        country = country,
        type = EnterpriseType(
            id = enterprise_type.id,
            typeName = enterprise_type.enterprise_type_name
        ),
    )
}