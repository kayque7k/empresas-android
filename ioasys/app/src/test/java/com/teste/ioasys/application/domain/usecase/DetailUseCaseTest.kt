package com.teste.ioasys.application.domain.usecase

import com.teste.ioasys.application.domain.model.Enterprise
import com.teste.ioasys.application.domain.repository.EnterprisesRepository
import com.teste.ioasys.application.usecase.EnterpriseDetailsUseCase
import com.teste.ioasys.commons.CoroutinesTestRule
import io.mockk.coEvery
import io.mockk.coVerify
import io.mockk.mockk
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.runBlocking
import kotlinx.coroutines.test.resetMain
import org.assertj.core.api.Assertions
import org.junit.After
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import com.teste.ioasys.commons.extensions.Result.Success

@ExperimentalCoroutinesApi
class DetailUseCaseTest {

    @get:Rule
    val coroutineTestRule = CoroutinesTestRule()

    private lateinit var useCase: EnterpriseDetailsUseCase
    private val repo: EnterprisesRepository = mockk(relaxed = true)

    @Before
    fun setup() {
        useCase = EnterpriseDetailsUseCase(repo)
    }

    @After
    fun tearDown() {
        Dispatchers.resetMain()
    }

    @Test
    fun `when execute use case return mock success`() {
        coEvery { repo.detail(0) } returns Enterprise.mock()

        val response = runBlocking { useCase.execute(0) }

        Assertions.assertThat(response).isEqualTo(Enterprise.mock())
        coVerify(exactly = 1) { repo.menu(any()) }
    }

    @Test
    fun `when execute use case return mock error`() {
        coEvery { repo.detail(0) } throws Exception(MOCK_EXCEPTION)

        Assertions.assertThatExceptionOfType(Exception::class.java)
            .isThrownBy {
                runBlocking {
                    useCase.execute(0)
                }
            }
            .withMessage(MOCK_EXCEPTION)
    }

    companion object {
        private const val MOCK_EXCEPTION = "Mock Exception"
    }
}