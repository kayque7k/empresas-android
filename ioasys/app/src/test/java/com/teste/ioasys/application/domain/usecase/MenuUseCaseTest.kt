package com.teste.ioasys.application.domain.usecase

import com.teste.ioasys.application.domain.model.Enterprise
import com.teste.ioasys.application.domain.repository.EnterprisesRepository
import com.teste.ioasys.application.usecase.EnterpriseMenuUseCase
import com.teste.ioasys.commons.CoroutinesTestRule
import com.teste.ioasys.commons.extensions.EMPTY_STRING
import io.mockk.coEvery
import io.mockk.coVerify
import io.mockk.mockk
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.runBlocking
import kotlinx.coroutines.test.resetMain
import org.assertj.core.api.Assertions
import org.junit.After
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import com.teste.ioasys.commons.extensions.Result.Success

@ExperimentalCoroutinesApi
class MenuUseCaseTest {

    @get:Rule
    val coroutineTestRule = CoroutinesTestRule()

    private lateinit var useCase: EnterpriseMenuUseCase
    private val repo: EnterprisesRepository = mockk(relaxed = true)

    @Before
    fun setup() {
        useCase = EnterpriseMenuUseCase(repo)
    }

    @After
    fun tearDown() {
        Dispatchers.resetMain()
    }

    @Test
    fun `when execute use case return mock success`() {
        coEvery { repo.menu(EMPTY_STRING) } returns Enterprise.mocks()

        val response = runBlocking { useCase.execute(EMPTY_STRING) }

        Assertions.assertThat(response).isEqualTo(Enterprise.mocks())
        coVerify(exactly = 1) { repo.menu(any()) }
    }

    @Test
    fun `when execute use case return mock error`() {
        coEvery { repo.menu(EMPTY_STRING) } throws Exception(MOCK_EXCEPTION)

        Assertions.assertThatExceptionOfType(Exception::class.java)
            .isThrownBy {
                runBlocking {
                    useCase.execute(EMPTY_STRING)
                }
            }
            .withMessage(MOCK_EXCEPTION)
    }

    companion object {
        private const val MOCK_EXCEPTION = "Mock Exception"
    }
}