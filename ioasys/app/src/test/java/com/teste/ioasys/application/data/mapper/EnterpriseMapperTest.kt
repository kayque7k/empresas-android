package com.teste.ioasys.application.data.mapper

import com.teste.ioasys.application.data.mapper.EnterpriseMapper.toEnterprise
import com.teste.ioasys.application.data.mapper.EnterpriseMapper.toEnterpriseList
import com.teste.ioasys.application.data.response.EnterpriseResponse
import com.teste.ioasys.application.domain.model.Enterprise
import org.assertj.core.api.Assertions
import org.junit.Test

class EnterpriseMapperTest {
    @Test
    fun `List EnterpriseResponse must map to toEnterpriseList`() {
        val mapperTest = EnterpriseResponse.mocks().toEnterpriseList()
        Assertions.assertThat(mapperTest.toString()).isEqualTo(Enterprise.mocks().toString())
    }

    @Test
    fun `EnterpriseResponse must map to toEnterprise`() {
        val mapperTest = EnterpriseResponse.mock().toEnterprise()
        Assertions.assertThat(mapperTest.toString()).isEqualTo(Enterprise.mock().toString())
    }

}