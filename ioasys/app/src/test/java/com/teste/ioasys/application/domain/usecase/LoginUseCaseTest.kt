package com.teste.ioasys.application.domain.usecase

import com.teste.ioasys.application.domain.repository.LoginRepository
import com.teste.ioasys.application.usecase.LoginUseCase
import com.teste.ioasys.commons.CoroutinesTestRule
import com.teste.ioasys.commons.extensions.EMPTY_STRING
import io.mockk.coEvery
import io.mockk.coVerify
import io.mockk.mockk
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.runBlocking
import kotlinx.coroutines.test.resetMain
import org.assertj.core.api.Assertions
import org.junit.After
import org.junit.Before
import org.junit.Rule
import org.junit.Test

@ExperimentalCoroutinesApi
class LoginUseCaseTest {

    @get:Rule
    val coroutineTestRule = CoroutinesTestRule()

    private lateinit var useCase: LoginUseCase
    private val repo: LoginRepository = mockk(relaxed = true)

    @Before
    fun setup() {
        useCase = LoginUseCase(repo)
    }

    @After
    fun tearDown() {
        Dispatchers.resetMain()
    }

    @Test
    fun `when execute use case return mock success`() {
        coEvery { repo.login(EMPTY_STRING, EMPTY_STRING) } returns Unit

        runBlocking { useCase.execute(EMPTY_STRING, EMPTY_STRING) }

        coVerify(exactly = 1) { repo.login(any(), any()) }
    }

    @Test
    fun `when execute use case return mock error`() {
        coEvery { repo.login(EMPTY_STRING, EMPTY_STRING) } throws Exception(MOCK_EXCEPTION)

        Assertions.assertThatExceptionOfType(Exception::class.java)
            .isThrownBy {
                runBlocking {
                    useCase.execute(EMPTY_STRING, EMPTY_STRING)
                }
            }
            .withMessage(MOCK_EXCEPTION)
    }

    companion object {
        private const val MOCK_EXCEPTION = "Mock Exception"
    }
}