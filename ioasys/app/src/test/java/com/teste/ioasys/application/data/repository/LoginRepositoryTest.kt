package com.teste.ioasys.application.data.repository

import com.teste.ioasys.application.data.api.LoginApi
import com.teste.ioasys.application.data.request.LoginRequest
import com.teste.ioasys.application.domain.repository.LoginRepository
import com.teste.ioasys.commons.extensions.EMPTY_STRING
import com.teste.ioasys.coreapi.SUCCESS_CODE
import io.mockk.coEvery
import io.mockk.coVerify
import io.mockk.mockk
import kotlinx.coroutines.runBlocking
import org.assertj.core.api.Assertions
import org.junit.Before
import org.junit.Test
import retrofit2.Response

class LoginRepositoryTest {

    private lateinit var repo: LoginRepository
    private val api: LoginApi = mockk(relaxed = true)

    private val responseMock: Response<Unit> = mockk(relaxed = true)

    @Before
    fun setup() {
        repo = LoginRepositoryImpl(api)
    }

    @Test
    fun `when call getLogin return mock success`() {
        coEvery { api.getLogin(LoginRequest()) } returns responseMock
        coEvery { responseMock.code() } returns SUCCESS_CODE

        runBlocking { repo.login(EMPTY_STRING, EMPTY_STRING) }

        coVerify(exactly = 1) { api.getLogin(any()) }
    }

    @Test
    fun `when call getLogin return mock error`() {
        coEvery { api.getLogin(LoginRequest()) } throws Exception(MOCK_EXCEPTION)

        Assertions.assertThatExceptionOfType(Exception::class.java)
            .isThrownBy {
                runBlocking {
                    repo.login(EMPTY_STRING, EMPTY_STRING)
                }
            }
            .withMessage(MOCK_EXCEPTION)
    }

    companion object {
        private const val MOCK_EXCEPTION = "Mock Exception"
    }

}