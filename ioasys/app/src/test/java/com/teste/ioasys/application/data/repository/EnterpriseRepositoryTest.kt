package com.teste.ioasys.application.data.repository

import com.teste.ioasys.application.data.api.EnterprisesApi
import com.teste.ioasys.application.data.mapper.EnterpriseMapper.toEnterprise
import com.teste.ioasys.application.data.mapper.EnterpriseMapper.toEnterpriseList
import com.teste.ioasys.application.data.response.ListReturnResponse
import com.teste.ioasys.application.data.response.ReturnResponse
import com.teste.ioasys.application.domain.repository.EnterprisesRepository
import com.teste.ioasys.commons.extensions.EMPTY_STRING
import io.mockk.coEvery
import io.mockk.coVerify
import io.mockk.mockk
import kotlinx.coroutines.runBlocking
import org.assertj.core.api.Assertions
import org.junit.Before
import org.junit.Test

class EnterpriseRepositoryTest {

    private lateinit var repo: EnterprisesRepository
    private val api: EnterprisesApi = mockk(relaxed = true)

    @Before
    fun setup() {
        repo = EnterprisesRepositoryImpl(api)
    }

    @Test
    fun `when call menu return mock success`() {
        coEvery { api.getEnterprises(EMPTY_STRING) } returns ListReturnResponse.mock()

        val response = runBlocking { repo.menu(EMPTY_STRING) }

        Assertions.assertThat(response).isEqualTo(ListReturnResponse.mock().enterprises.toEnterpriseList())
        coVerify(exactly = 1) { api.getEnterprises(any()) }
    }

    @Test
    fun `when call menu return mock error`() {
        coEvery { api.getEnterprises(EMPTY_STRING) } throws Exception(MOCK_EXCEPTION)

        Assertions.assertThatExceptionOfType(Exception::class.java)
            .isThrownBy {
                runBlocking {
                    repo.menu(EMPTY_STRING)
                }
            }
            .withMessage(MOCK_EXCEPTION)
    }

    @Test
    fun `when call getEnterprise return mock success`() {
        coEvery { api.getEnterprise(1) } returns ReturnResponse.mock()

        val response = runBlocking { repo.detail(1) }

        Assertions.assertThat(response).isEqualTo(ReturnResponse.mock().enterprise.toEnterprise())
        coVerify(exactly = 1) { api.getEnterprise(any()) }
    }

    @Test
    fun `when call getEnterprise return mock error`() {
        coEvery { api.getEnterprise(1) } throws Exception(MOCK_EXCEPTION)

        Assertions.assertThatExceptionOfType(Exception::class.java)
            .isThrownBy {
                runBlocking {
                    repo.detail(1)
                }
            }
            .withMessage(MOCK_EXCEPTION)
    }

    companion object {
        private const val MOCK_EXCEPTION = "Mock Exception"
    }

}